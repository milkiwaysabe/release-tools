module ReleaseManagers
  autoload :Client,      'release_managers/client'
  autoload :Definitions, 'release_managers/definitions'
end
